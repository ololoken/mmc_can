//based on antonio-dj(forum 4pda.ru) mmc_can.dll v2.8
#pragma once

#include <windows.h>

#define CAN_BUFF_WRITE_SIZE 128
#define CAN_BUFF_READ_SIZE 8192
#define CAN_MSG_LENGTH 16
#define CAN_MSG_PAYLOAD_LENGTH 8

#define CAN_ERR_NONE 0
#define CAN_ERR_INITED -1001
#define CAN_ERR_PORT -1002
#define CAN_ERR_THREAD -1003
#define CAN_ERR_BUSY -1004

#define CAN_ACTIVITY_TIMEOUT 200
#define CAN_DISTANCE_UPDATE_RATE 1000

#ifdef _DEBUG
#define CAN_PORT_NAME _T("COM1:")
#define CAN_PORT_BRATE CBR_600
#else
#define CAN_PORT_NAME _T("COM3:")
#define CAN_PORT_BRATE CBR_115200
#endif

#define CAN_MSG_ECU 0x0551 //������a����� ������� ���������� ���������� ����
#define CAN_MSG_ECU2 0x02DE
#define CAN_MSG_ICU 0x0280 //���������� ��������
#define CAN_MSG_TCU 0x0560 //���������� �����������
#define CAN_MSG_ACU 0x0498 //���������� ������� ����
#define CAN_MSG_ABS 0x0354 //���������� ABS
#define CAN_MSG_CBE 0x0481 //���� �������� ����������� ����
#define CAN_MSG_CCU 0x0555 //���������� �������
#define CAN_MSG_ECURPM 0x0180
//������ ��������� gps � ����������� ������� (-:
#define CAN_MSG_NMEA_1 0x04A4
#define CAN_MSG_NMEA_2 0x04A6
#define CAN_MSG_NMEA_3 0x04A8
#define CAN_MSG_NMEA_4 0x04AA
#define CAN_MSG_NMEA_5 0x04AC
#define CAN_MSG_NMEA_6 0x04AE

#define WM_CAN_CHANGE_ENGINE_TEMP (WM_USER+64)
#define WM_CAN_CHANGE_FUEL (WM_USER+65)
#define WM_CAN_CHANGE_GEARBOX_TEMP (WM_USER+66)
#define WM_CAN_CHANGE_AIR_TEMP (WM_USER+67)
#define WM_CAN_CHANGE_BAT_CHARGE_FLAG (WM_USER+68)
#define WM_CAN_CHANGE_WARN_ENG_TEMP_FLAG (WM_USER+69)
#define WM_CAN_CHANGE_WARN_OIL_PRES_FLAG (WM_USER+70)
#define WM_CAN_CHANGE_CHECK_ENGINE_FLAG (WM_USER+71)
#define WM_CAN_CHANGE_ACTIVE (WM_USER+72)
#define WM_CAN_CHANGE_SPEED (WM_USER+73)
#define WM_CAN_CHANGE_SAFETY_BELT_FLAG (WM_USER+74)
#define WM_CAN_CHANGE_ECU_ACTIVE (WM_USER+75)
#define WM_CAN_CHANGE_SRS_ACTIVE (WM_USER+76)
#define WM_CAN_CHANGE_BREAK_SWITCH_STATE (WM_USER+77)
#define WM_CAN_CHANGE_REAR_WINDOW_HEATING_STATE (WM_USER+78)
#define WM_CAN_CHANGE_FRONT_WINDOW_HEATING_STATE (WM_USER+79)
#define WM_CAN_CHANGE_LOW_BEAM_STATE (WM_USER+80)
#define WM_CAN_CHANGE_HIGH_BEAM_STATE (WM_USER+81)
#define WM_CAN_CHANGE_DRIVERS_DOOR_STATE (WM_USER+82)
#define WM_CAN_CHANGE_PASSENGERS_DOOR_STATE (WM_USER+83)
#define WM_CAN_CHANGE_REAR_DOORS_STATE (WM_USER+84)
//#define WM_CAN_CHANGE_HOOD_STATE (WM_USER+85)
//#define WM_CAN_CHANGE_TAILGATE_STATE (WM_USER+86)
#define WM_CAN_CHANGE_RIGHT_TURN_STATE (WM_USER+87)
#define WM_CAN_CHANGE_LEFT_TURN_STATE (WM_USER+88)
#define WM_CAN_CHANGE_IN_CAR_TEMP (WM_USER+89)
#define WM_CAN_CHANGE_TARGET_TEMP (WM_USER+90)
#define WM_CAN_CHANGE_ECU_FUEL_CONSUMPTION_ACTIVE (WM_USER+91)
#define WM_CAN_CHANGE_FUEL_INSTANT_CONSUMPTION (WM_USER+92)
//from 2.9 antonio-dj 4pda.ru
#define WM_CAN_CHANGE_AIR_FLOW_DIRECTION (WM_USER+93)
#define WM_CAN_CHANGE_AIR_FLOW_SPEED (WM_USER+94)
#define WM_CAN_CHANGE_RPM (WM_USER+95)
#define WM_CAN_CHANGE_OBD_PID_MSG (WM_USER+96)
#define WM_CAN_CHANGE_CONDITIONER_STATE (WM_USER+97)
#define WM_CAN_CHANGE_RECIRCULATION_STATE (WM_USER+98)
#define WM_CAN_CHANGE_WINDSHIELD_FLOW_STATE (WM_USER+99)
#define WM_CAN_READ_OBD2_ERRORS (WM_USER+100)
#define WM_CAN_CHANGE_PARK_STATE (WM_USER+101)
#define WM_CAN_CHANGE_MARKER_LIGHT_STATE (WM_USER+102)
#define WM_CAN_CHANGE_IGNITION_STATE (WM_USER+103)
#define WM_CAN_CHANGE_DISTANCE (WM_USER+104)

#define WM_CAN_PORT_ERROR (WM_USER+1000)
#define WM_CAN_PORT_EMULATOR (WM_USER+1001)
#define WM_CAN_UNKNOWN (WM_USER+1002)
#define WM_CAN_RUNNING_STATE (WM_USER+1003)
#define WM_CAN_LOG_BYTES (WM_USER+1004)
#define WM_CAN_VCOM_DEV (WM_USER+1005)
#define WM_CAN_NMEA (WM_USER+1006)

#define CAN_CCU_TEMP_OFFSET 13//temp offset for climatic system
#define CAN_ECU_TEMP_OFFSET -40//temp offset for ECU temp data
#define CAN_ECU_FUEL_CONSUPTION_MULTIPLIER 466.2f
#define CAN_ECU_FUEL_TOTAL_MULTIPLIER 2.0f
#define CAN_ECU_SPEED_MULTIPLIER 100.0f

#ifdef MMC_CAN_EXPORTS
#define MMC_CAN_API __declspec(dllexport)
#else
#define MMC_CAN_API __declspec(dllimport)
#endif

typedef void (*CAN_API_Callback)(BYTE*, size_t);

typedef struct VEHICLE {
  signed long enginetemp;
  signed long gearboxtemp;
  signed long airtemp;
  unsigned long speed;
  unsigned long consumption_counter_10ml;
  unsigned long cur_consumption;
  unsigned long cur_consumption_last;
  unsigned long trip_m;
  double trip;
  long fuel;
  long batterycharge;
  long checkengine;
  int canactive;
  DWORD canactivemark;
  long lowoilpressure;
  long highenginetemperature;
  long safetybelt;
  int ecuactive;
  DWORD ecuactivemark;
  int ecuactive_fuelconsumption;
  DWORD fuelconsumptionactivemark;
  BYTE ecu_msg_counter;
  int srsactive;
  DWORD srsactivemark;
  long break_switch_on;
  long rear_window_heating;
  long front_window_heating;
  long low_beam_light;
  long high_beam_light;
  long drivers_door_opened;
  long passangers_door_opened;
  long rear_doors_opened;
  //long hood_opened;
  //long tailgate_opened;
  long right_turn_light;
  long left_turn_light;
  signed long incartemp;
  signed long targettemp;
  long climatic_on;
  unsigned long inst_consumption;
  unsigned long air_flow_direction;
  unsigned long air_flow_speed;
  long rpm;
  long conditioner_state;
  long recirculation_state;
  long windshield_flow_state;
  long park_state;
  long marker_light_state;
} VEHICLE, *LPVEHICLE;

#ifdef __cplusplus 
extern "C" {
#endif
MMC_CAN_API int CAN_Init (HWND msgHWND);
MMC_CAN_API int CAN_Close ();
MMC_CAN_API int CAN_LogStart (const char* fileName);
MMC_CAN_API int CAN_LogStop ();
MMC_CAN_API int CAN_VCOMInit ();
MMC_CAN_API int CAN_VCOMClose ();
MMC_CAN_API int CAN_GetVehicleData (VEHICLE* v);
MMC_CAN_API int CAN_ResetECUErrors ();
MMC_CAN_API int CAN_ResetEngineECU (BYTE mode);
MMC_CAN_API int CAN_Fan1On ();
MMC_CAN_API int CAN_Fan1Off ();
MMC_CAN_API int CAN_Fan2On ();
MMC_CAN_API int CAN_Fan2Off ();
MMC_CAN_API int CAN_SetIdleRPM (BYTE rpm);
MMC_CAN_API int CAN_ResetABSErrors ();
MMC_CAN_API int CAN_ResetSRSErrors ();
MMC_CAN_API int CAN_ResetTCUErrors ();
MMC_CAN_API int CAN_TesterPresent ();
#ifdef __cplusplus 
}
#endif