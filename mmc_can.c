#include "mmc_can.h"
#include "vcom/vcom.h"

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

#define V_PORT_NUM 5

typedef struct CANDelayedMsg {
  BYTE* payload;
  size_t length;
  DWORD timeout;
} CANDelayedMsg;

VEHICLE theVehicle = {0};

int g_inited = FALSE;
BOOL g_emulator = FALSE;
CAN_API_Callback g_rawDataCb;
HANDLE g_hPort = NULL;
HWND g_HWND_dest = NULL;
HANDLE DllPortReadThread = NULL;
HANDLE DllUpdateThread = NULL;
FILE* g_logFile = NULL;
FILE* g_GPSData = NULL;
BOOL g_comsending = FALSE;
DWORD g_vcomPort = 0;

CRITICAL_SECTION g_bufOperation;
CRITICAL_SECTION g_logOperation;
static BYTE g_comBuf[CAN_BUFF_READ_SIZE<<1];
static int g_comBufLen = 0;

#pragma region activity
__inline int CANActive (int state) {
  theVehicle.canactivemark = GetTickCount();
  if (theVehicle.canactive != state) {
    PostMessage(g_HWND_dest, WM_CAN_CHANGE_ACTIVE, (WPARAM)state, 0);
  }
  return theVehicle.canactive = state;
}

__inline int ECUActive (int state) {
  theVehicle.ecuactivemark = GetTickCount();
  if (theVehicle.ecuactive != state) {
    PostMessage(g_HWND_dest, WM_CAN_CHANGE_ECU_ACTIVE, (WPARAM)state, 0);
  }
  return theVehicle.ecuactive = state;
}

__inline int ECUFuelActive (int state) {
  theVehicle.fuelconsumptionactivemark = GetTickCount();
  if (theVehicle.ecuactive_fuelconsumption != state) {
    PostMessage(g_HWND_dest, WM_CAN_CHANGE_ECU_FUEL_CONSUMPTION_ACTIVE, (WPARAM)state, 0);
  }
  return theVehicle.ecuactive_fuelconsumption = state;
}


__inline int SRSActive (int state) {
  theVehicle.srsactivemark = GetTickCount();
  if (theVehicle.srsactive != state) {
    PostMessage(g_HWND_dest, WM_CAN_CHANGE_SRS_ACTIVE, (WPARAM)state, 0);
  }
  return theVehicle.srsactive = state;
}
#pragma endregion activity

//func protos
static DWORD WINAPI ReadPortAsyncThreadProc (LPVOID lpParameter);
static DWORD WINAPI BufferAnalyzeThreadProc (LPVOID lpParameter);
BOOL OpenPort ();
void ClosePort();
BOOL BufAnalyze (const BYTE* buffer, const DWORD len);
BOOL PortWriteWithTimeout (BYTE *payload, size_t length, DWORD timeout);
void UpdateCurrentTickCount ();
void UpdateStatuses ();
void UpdateDistance ();

static __inline BOOL theVehicleUpdate (long *prop, long value, UINT msg) {
  if (*prop != value) {
    *prop = value;
    PostMessage(g_HWND_dest, msg, (WPARAM)value, 0);
    return TRUE;
  }
  return FALSE;
}

static __inline BOOL theVehicleUpdateLong (long *prop, long value, UINT msg, LPARAM lParam) {
  if (*prop != value) {
    *prop = value;
    PostMessage(g_HWND_dest, msg, (WPARAM)value, lParam);
    return TRUE;
  }
  return FALSE;
}

int InitInstance (HWND msgTarget) {
  wchar_t drvPath[MAX_PATH] = {0};
  if (g_inited) {
    return CAN_ERR_INITED;
  }
  
  g_emulator = FALSE;

  theVehicle.canactive = FALSE;
  theVehicle.ecuactive = FALSE;
  theVehicle.srsactive = FALSE;

  theVehicle.trip_m = 0;
  theVehicle.trip = 0;
  theVehicle.cur_consumption = 0;

  g_hPort = NULL;
  g_HWND_dest = msgTarget;

  theVehicle.enginetemp = -1000;
  theVehicle.gearboxtemp = -1000;
  theVehicle.airtemp = -1000;
  theVehicle.speed = 0;
  theVehicle.consumption_counter_10ml = 0;
  theVehicle.fuel = -1000;

  if (!OpenPort()) {
    return CAN_ERR_PORT;
  }
  
  DllUpdateThread = CreateThread(NULL, 0, &BufferAnalyzeThreadProc, NULL, 0, NULL);
  if (NULL == DllUpdateThread) {
    return CAN_ERR_THREAD;
  }
  DllPortReadThread = CreateThread(NULL, 0, &ReadPortAsyncThreadProc, NULL, 0, NULL);
  if (NULL == DllPortReadThread) {
    return CAN_ERR_THREAD;
  }
  g_inited = TRUE;

  InitializeCriticalSection(&g_bufOperation);
  InitializeCriticalSection(&g_logOperation);

  return CAN_ERR_NONE;
}
//*****************************************************************************
int ExitInstance () {
  ClosePort();
  CloseHandle(DllPortReadThread);
  CloseHandle(DllUpdateThread);
  g_inited = FALSE;
  return CAN_ERR_NONE;
}
//*****************************************************************************

BOOL OpenPort () {
  LPCWSTR comname;
  DWORD dwerrors;
  BOOL fSuccess;
  COMMTIMEOUTS PortTimeouts;
  COMSTAT comstat;
  COMMPROP comprop;
  DCB dcb;
  float fOneCharTime;

  comname = CAN_PORT_NAME;
  if (g_hPort) {
    CloseHandle(g_hPort);
    g_hPort = NULL;
  }
  if(!g_hPort) {
    g_hPort = CreateFile(comname, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
    if (g_hPort == INVALID_HANDLE_VALUE) {
      dwerrors = GetLastError();
      PostMessage(g_HWND_dest, WM_CAN_PORT_ERROR, (WPARAM)dwerrors, 0);
      g_hPort = NULL;
      return FALSE;
    }
  }
  dcb.DCBlength = sizeof(DCB);
  fSuccess = GetCommProperties(g_hPort, &comprop);
  if(!fSuccess) {
    CloseHandle(g_hPort);
    g_hPort = NULL;
    return FALSE;
  }

  GetCommState(g_hPort, &dcb);
  EscapeCommFunction(g_hPort, CLRRTS);
  EscapeCommFunction(g_hPort, CLRDTR);
  dcb.BaudRate = CAN_PORT_BRATE;     //  baud rate
  dcb.fBinary = TRUE;
  dcb.Parity   = NOPARITY;      //  parity bit
  dcb.fOutxCtsFlow = FALSE;
  dcb.fOutxDsrFlow = FALSE;
  dcb.ByteSize = 8;             //  data size, xmit and rcv
  dcb.StopBits = ONESTOPBIT;    //  stop bit
  dcb.fDtrControl = DTR_CONTROL_DISABLE;
  dcb.fRtsControl = RTS_CONTROL_DISABLE;
  dcb.fDsrSensitivity = FALSE;
  dcb.fTXContinueOnXoff = FALSE;
  dcb.fOutX = FALSE;
  dcb.fInX = FALSE;
  dcb.fErrorChar = FALSE;
  dcb.fNull = FALSE;
  dcb.fAbortOnError = FALSE;
  dcb.XoffLim = 4096;
  dcb.XoffLim = 4096;
  dcb.XonChar = 0;
  dcb.XoffChar = 0;
  dcb.ErrorChar = 0;
  dcb.EofChar = 0;
  dcb.EvtChar = 0;
  fSuccess = SetCommState(g_hPort, &dcb);
  
  if (!fSuccess) {//emulator
    g_emulator = 1;
  }
  PostMessage(g_HWND_dest, WM_CAN_PORT_EMULATOR, (WPARAM)g_emulator, 0);
  if (comprop.dwMaxRxQueue == 0) {
    comprop.dwMaxRxQueue = 4096;
  }
  if (comprop.dwMaxTxQueue == 0) {
    comprop.dwMaxTxQueue = 4096;
  }
  SetupComm(g_hPort, comprop.dwMaxRxQueue, comprop.dwMaxTxQueue);

  fOneCharTime = 1/(float)dcb.BaudRate*100000;

  PortTimeouts.ReadIntervalTimeout = (DWORD)ceil(fOneCharTime*3);
  PortTimeouts.ReadTotalTimeoutMultiplier = (DWORD)ceil(fOneCharTime*2);
  PortTimeouts.ReadTotalTimeoutConstant = (DWORD)ceil(fOneCharTime*3);
  PortTimeouts.WriteTotalTimeoutMultiplier = PortTimeouts.ReadTotalTimeoutMultiplier;
  PortTimeouts.WriteTotalTimeoutConstant = PortTimeouts.ReadTotalTimeoutConstant;
  SetCommTimeouts(g_hPort, &PortTimeouts);

  ClearCommBreak(g_hPort);
  PurgeComm(g_hPort, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR); // ������� �������
  ClearCommError(g_hPort, &dwerrors, &comstat);

  return TRUE;
}



void PortReadAsync () {
  BYTE byte;
  DWORD dwCommModemStatus = 0, dwBytesTransferred = 0;
  SetCommMask(g_hPort, EV_RXCHAR|EV_CTS|EV_DSR|EV_RLSD|EV_RING);
  while (g_hPort && INVALID_HANDLE_VALUE != g_hPort) {
    WaitCommEvent(g_hPort, &dwCommModemStatus, 0);
    SetCommMask(g_hPort, EV_RXCHAR|EV_CTS|EV_DSR|EV_RING);
    if (dwCommModemStatus & EV_RXCHAR) {
      do {
        ReadFile(g_hPort, &byte, 1, &dwBytesTransferred, 0);
        if (dwBytesTransferred == 1) {
          EnterCriticalSection(&g_bufOperation);
          if (CAN_BUFF_READ_SIZE == g_comBufLen) {
            g_comBufLen = 0;
          }
          g_comBuf[g_comBufLen++] = byte;
          LeaveCriticalSection(&g_bufOperation);
        }
      } while (dwBytesTransferred == 1);
    }
  }
}

//actually nmea max sentence size is 82 bytes
#define BUF_NMEA_MAX_SIZE 512
#define NMEA_SENTENCE_MAX_SIZE 82

void ProcessNMEA (const BYTE *nmea) {
  static BYTE nmeaBuf[BUF_NMEA_MAX_SIZE] = {0};
  static nmeaBufLen = 0;
  const static BYTE
    START[] = {0x24},
    END[] = {0x0D, 0x0A};
  BYTE comMsg[VCOMDRIVER_WRITE_SIZE] = {0};
  BYTE *start = NULL, *end = NULL;
  size_t msgLen;
  int startIdx = 0, endIdx = 0, i = 0;
  do {
    nmeaBuf[nmeaBufLen++] = nmea[i++];
  } while (i < CAN_MSG_PAYLOAD_LENGTH && nmea[i] && nmeaBufLen < BUF_NMEA_MAX_SIZE);
  if (nmeaBufLen >= BUF_NMEA_MAX_SIZE) {//purge broken buffer
    memset(&nmeaBuf[0], 0x00, BUF_NMEA_MAX_SIZE);
    nmeaBufLen = 0;
    return;
  }
  start = strstr(nmeaBuf, START);
  end = strstr(nmeaBuf, END);
  if (start && end) {
    startIdx = (start-nmeaBuf);
    endIdx = (end-nmeaBuf);
    msgLen = endIdx-startIdx+2;//2 bytes for CRLF
    if (msgLen <= VCOMDRIVER_WRITE_SIZE) {
      memcpy(&comMsg[0], &nmeaBuf[startIdx], msgLen);
      VCOM_Write(V_PORT_NUM, comMsg, msgLen);
      PostMessage(g_HWND_dest, WM_CAN_NMEA, (WPARAM)msgLen, 0);
    }
  }
  if (end) {
    nmeaBufLen = 0;
    memset(&nmeaBuf[0], 0x00, BUF_NMEA_MAX_SIZE);
  }
}

int BufAnalyze (const BYTE* buffer, const DWORD len) {
  static BYTE comBuf[CAN_BUFF_READ_SIZE];
  static int chunkLen = 0;
  int j = 0;
  int bufTotalLen = (int)len+chunkLen;
  //�������� � ������������� �����
  memcpy(&comBuf[chunkLen], &buffer[0], (size_t)len);

  for (j = 0; j <= bufTotalLen-CAN_MSG_LENGTH;) {
    if((comBuf[j]+comBuf[j+1]+comBuf[j+2]+comBuf[j+3]) == 0x00) {//CAN message start
      CANActive(1);
      switch ((comBuf[j+4]<<8)+(comBuf[j+5]<<0)) {//get message id
        case CAN_MSG_ECU: {
          ECUActive(1);
          theVehicleUpdate(&theVehicle.enginetemp, (long)comBuf[j+7], WM_CAN_CHANGE_ENGINE_TEMP);
          theVehicleUpdate(&theVehicle.batterycharge, (long)(comBuf[j+9] & 0x01), WM_CAN_CHANGE_BAT_CHARGE_FLAG);
          theVehicleUpdate(&theVehicle.checkengine, (long)(comBuf[j+10] & 0x03), WM_CAN_CHANGE_CHECK_ENGINE_FLAG);
          theVehicleUpdate(&theVehicle.lowoilpressure, (long)((comBuf[j+10] >> 2) & 0x01), WM_CAN_CHANGE_WARN_OIL_PRES_FLAG);
          theVehicleUpdate(&theVehicle.highenginetemperature, (((long)comBuf[j+10] >> 3) & 0x01), WM_CAN_CHANGE_WARN_ENG_TEMP_FLAG);
          theVehicle.cur_consumption = (long)comBuf[j+8];
          if(theVehicle.cur_consumption_last != theVehicle.cur_consumption) {
            ECUFuelActive(1);
            if(theVehicle.cur_consumption_last > theVehicle.cur_consumption) { 
              theVehicle.consumption_counter_10ml++;
            }
            theVehicle.cur_consumption_last = theVehicle.cur_consumption;
          }
          theVehicleUpdate(&theVehicle.inst_consumption, (unsigned long)comBuf[j+13]|((unsigned long)comBuf[j+12]<<8), WM_CAN_CHANGE_FUEL_INSTANT_CONSUMPTION);
        } break;
        case CAN_MSG_ICU: {
          static DWORD ticks = 0;
          theVehicleUpdate(&theVehicle.fuel, (long)comBuf[j+7], WM_CAN_CHANGE_FUEL);
          theVehicleUpdate(&theVehicle.airtemp, (long)comBuf[j+8], WM_CAN_CHANGE_AIR_TEMP);
          theVehicleUpdate(&theVehicle.speed, (unsigned long)comBuf[j+11]|((unsigned long)comBuf[j+10]<<8), WM_CAN_CHANGE_SPEED);

          theVehicle.trip += theVehicle.speed*(GetTickCount()-ticks)/360000;
          theVehicleUpdate(&theVehicle.trip_m, (long)(0.5f+theVehicle.trip), WM_CAN_CHANGE_DISTANCE);
          ticks = GetTickCount();
        } break;
        case CAN_MSG_TCU: {
          theVehicleUpdate(&theVehicle.gearboxtemp, (long)comBuf[j+6], WM_CAN_CHANGE_GEARBOX_TEMP);
        } break;
        case CAN_MSG_ACU: {
          SRSActive(1);
          theVehicleUpdate(&theVehicle.safetybelt, ((long)comBuf[j+6] & 0x03), WM_CAN_CHANGE_SAFETY_BELT_FLAG);
        } break;
        case CAN_MSG_ABS: {
          theVehicleUpdate(&theVehicle.break_switch_on, ((long)comBuf[j+6] & 0x01), WM_CAN_CHANGE_BREAK_SWITCH_STATE);
        } break;
        case CAN_MSG_CBE: {
          theVehicleUpdate(&theVehicle.rear_window_heating, (((long)comBuf[j+9] >> 1) & 0x01), WM_CAN_CHANGE_REAR_WINDOW_HEATING_STATE);
          theVehicleUpdate(&theVehicle.front_window_heating, (((long)comBuf[j+9] >> 1) & 0x01), WM_CAN_CHANGE_REAR_WINDOW_HEATING_STATE);
          
          theVehicleUpdate(&theVehicle.low_beam_light, (((long)comBuf[j+6] >> 6) & 0x01), WM_CAN_CHANGE_LOW_BEAM_STATE);
          theVehicleUpdate(&theVehicle.high_beam_light, (((long)comBuf[j+6] >> 4) & 0x01), WM_CAN_CHANGE_HIGH_BEAM_STATE);
          
          theVehicleUpdate(&theVehicle.drivers_door_opened, (((long)comBuf[j+6] >> 7) & 0x01), WM_CAN_CHANGE_DRIVERS_DOOR_STATE);
          theVehicleUpdate(&theVehicle.passangers_door_opened, (((long)comBuf[j+6] >> 5) & 0x01), WM_CAN_CHANGE_PASSENGERS_DOOR_STATE);
          theVehicleUpdate(&theVehicle.rear_doors_opened, (((long)comBuf[j+7] >> 4) & 0x01), WM_CAN_CHANGE_REAR_DOORS_STATE);

          theVehicleUpdate(&theVehicle.right_turn_light, (((long)comBuf[j+8] >> 0) & 0x01), WM_CAN_CHANGE_RIGHT_TURN_STATE);
          theVehicleUpdate(&theVehicle.left_turn_light, (((long)comBuf[j+8] >> 1) & 0x01), WM_CAN_CHANGE_LEFT_TURN_STATE);
        } break;
        case CAN_MSG_CCU: {
          theVehicleUpdate(&theVehicle.incartemp, ((signed long)comBuf[j+6]), WM_CAN_CHANGE_IN_CAR_TEMP);
          theVehicleUpdate(&theVehicle.targettemp, (signed long)(comBuf[j+7] & 0x1F), WM_CAN_CHANGE_TARGET_TEMP);
          theVehicleUpdate(&theVehicle.air_flow_direction, (unsigned long)((comBuf[j+7] & 0xE0) >> 5), WM_CAN_CHANGE_AIR_FLOW_DIRECTION);
          theVehicleUpdateLong(&theVehicle.air_flow_speed, (unsigned long)(comBuf[j+8] & 0x07), WM_CAN_CHANGE_AIR_FLOW_SPEED, (unsigned long)(comBuf[j+8] | (comBuf[j+9] << 8)));
          theVehicleUpdate(&theVehicle.conditioner_state, (unsigned long)((comBuf[j+8] & 0x08) >> 3), WM_CAN_CHANGE_CONDITIONER_STATE);
          theVehicleUpdate(&theVehicle.recirculation_state, (unsigned long)((comBuf[j+8] & 0x10) >> 4), WM_CAN_CHANGE_RECIRCULATION_STATE);
          theVehicleUpdate(&theVehicle.windshield_flow_state, (unsigned long)((comBuf[j+9] & 0x08) >> 3), WM_CAN_CHANGE_WINDSHIELD_FLOW_STATE);
        } break;
        case CAN_MSG_ECURPM: {
          theVehicleUpdate(&theVehicle.rpm, ((unsigned long)comBuf[j+7] + ((unsigned long)comBuf[j+6] << 8))/8, WM_CAN_CHANGE_RPM);
        } break;
        case CAN_MSG_ECU2: {
          theVehicleUpdate(&theVehicle.park_state, (long)(comBuf[j+8] & 0x01), WM_CAN_CHANGE_PARK_STATE);
          theVehicleUpdate(&theVehicle.marker_light_state, (long)((comBuf[j+8] & 0x04) >> 2), WM_CAN_CHANGE_MARKER_LIGHT_STATE);
        } break;
        case CAN_MSG_NMEA_1:
        case CAN_MSG_NMEA_2:
        case CAN_MSG_NMEA_3:
        case CAN_MSG_NMEA_4:
        case CAN_MSG_NMEA_5:
        case CAN_MSG_NMEA_6: if (g_vcomPort) {
          BYTE bpNMEA[CAN_MSG_PAYLOAD_LENGTH] = {0};
          memcpy(&bpNMEA[0], &comBuf[j+6], CAN_MSG_PAYLOAD_LENGTH);
          ProcessNMEA(bpNMEA);
        } break;
        default: {
          //todo: �������� ������ ��������� � ���� ������ � Init
        }
      }
      if (g_logFile) {
        BYTE bpCanMSG[CAN_MSG_LENGTH];
        memcpy(&bpCanMSG[0], &comBuf[j], CAN_MSG_LENGTH);
        EnterCriticalSection(&g_logOperation);
        fwrite(bpCanMSG, CAN_MSG_LENGTH, 1, g_logFile);
        LeaveCriticalSection(&g_logOperation);
        PostMessage(g_HWND_dest, WM_CAN_LOG_BYTES, (WPARAM)CAN_MSG_LENGTH, 0);
      }
      j += CAN_MSG_LENGTH;//����������� ����� ���������
    }
    else {
      ++j;
    }
  }
  //shift buffer
  chunkLen = bufTotalLen-j;
  memcpy(&comBuf[0], &comBuf[j], chunkLen);
  return chunkLen;
}

void PrepareMessage (BYTE* msg, size_t length, ...) {
  DWORD msg_summ = 0;
  size_t i;
  va_list vl;
  va_start(vl, length);

  for (i = 0; i < length; ++i) {
    msg[i] = va_arg(vl, BYTE);
    msg_summ += msg[i];
  }
  //set crc
  msg_summ = 0x10000 - msg_summ;
  msg[14] = (BYTE)((msg_summ >> 8) & 0xFF); // CRC H Byte
  msg[15] = (BYTE)(msg_summ & 0xFF); // CRC L Byte
}

//*****************************************************************************
BOOL PortWrite (BYTE* payload, size_t length) {
  BOOL fSuccess = FALSE;
  DWORD len = 0;

  if(!g_hPort) {
    return FALSE;
  }
  fSuccess = WriteFile(g_hPort, payload, MIN(length, CAN_BUFF_WRITE_SIZE), &len, 0);  // ���������� �����
  return fSuccess;
}

DWORD WINAPI TimeoutWriterProc (LPVOID lpParameter) {
  CANDelayedMsg* msg = (CANDelayedMsg*)lpParameter;
  BOOL res = FALSE;
  Sleep(msg->timeout);
  res = PortWrite(msg->payload, msg->length);
  g_comsending = FALSE;
  ExitThread(FALSE == res);
  return FALSE == res;
}

BOOL PortWriteWithTimeout (BYTE *payload, size_t length, DWORD timeout) {
  CANDelayedMsg msg;
  HANDLE h;
  if (g_comsending) {
    return 0;
  }

  g_comsending = TRUE;
  msg.timeout = timeout;
  msg.length = length;
  msg.payload = payload;
  h = CreateThread(0, 0, &TimeoutWriterProc, &msg, 0, NULL);
  return h != NULL;
  
}
//*****************************************************************************
void ClosePort () {
  if((g_hPort != 0) && (g_hPort != INVALID_HANDLE_VALUE)) {
    CloseHandle(g_hPort);
    g_hPort = NULL;
  }
}

DWORD WINAPI ReadPortAsyncThreadProc (LPVOID lpParameter) {
  PostMessage(g_HWND_dest, WM_CAN_RUNNING_STATE, (WPARAM)1, 0);
  PortReadAsync();
  PostMessage(g_HWND_dest, WM_CAN_RUNNING_STATE, (WPARAM)0, 0);
  ExitThread(0);
  return 0;
}

DWORD WINAPI BufferAnalyzeThreadProc (LPVOID lpParameter) {
  while (g_hPort) {
    Sleep(50);
    EnterCriticalSection(&g_bufOperation);
    BufAnalyze(g_comBuf, g_comBufLen);
    memset(&g_comBuf[0], 0x00, g_comBufLen); 
    g_comBufLen = 0;
    LeaveCriticalSection(&g_bufOperation);
    UpdateStatuses();
  }
  ExitThread(0);
  return 0;
}

void UpdateStatuses () {
  DWORD CurrentTickCount = GetTickCount();
  if (theVehicle.canactive && (CurrentTickCount - theVehicle.canactivemark) > CAN_ACTIVITY_TIMEOUT) {
    CANActive(0);
  }
  if (theVehicle.ecuactive && (CurrentTickCount - theVehicle.ecuactivemark) > CAN_ACTIVITY_TIMEOUT) {
    ECUActive(0);
  }
  if (theVehicle.srsactive && (CurrentTickCount - theVehicle.srsactivemark) > CAN_ACTIVITY_TIMEOUT) {
    SRSActive(0);
  }
  if (theVehicle.ecuactive_fuelconsumption && (CurrentTickCount - theVehicle.fuelconsumptionactivemark) > CAN_ACTIVITY_TIMEOUT) {
    ECUFuelActive(0);
  }
}

#ifdef __cplusplus 
extern "C" {
#endif

MMC_CAN_API int CAN_Init (HWND msgHWND) {
  return InitInstance(msgHWND);
}

MMC_CAN_API int CAN_VCOMInit () {
  //try to init virtual com driver
  g_vcomPort = VCOM_Init(V_PORT_NUM);
  if (g_vcomPort) {
    PostMessage(g_HWND_dest, WM_CAN_VCOM_DEV, (WPARAM)1, (LPARAM)g_vcomPort);
  }
  else {
    PostMessage(g_HWND_dest, WM_CAN_VCOM_DEV, (WPARAM)0, (LPARAM)GetLastError());
    return CAN_ERR_PORT;
  }
  return CAN_ERR_NONE;
}

MMC_CAN_API int CAN_VCOMClose () {
  PostMessage(g_HWND_dest, WM_CAN_VCOM_DEV, (WPARAM)0, (LPARAM)0);
  VCOM_DeInit(V_PORT_NUM);
  g_vcomPort = 0;
  return CAN_ERR_NONE;
}

MMC_CAN_API int CAN_Close () {
  CAN_LogStop();
  CAN_VCOMClose();
  return ExitInstance();
}

MMC_CAN_API int CAN_LogStart (const char* fileName) {
  EnterCriticalSection(&g_logOperation);
  g_logFile = fopen(fileName, "wb");
  LeaveCriticalSection(&g_logOperation);
  return NULL != g_logFile ? CAN_ERR_NONE : GetLastError();
}

MMC_CAN_API int CAN_LogStop () {
  if (g_logFile) {
    EnterCriticalSection(&g_logOperation);
    fflush(g_logFile);
    fclose(g_logFile);
    g_logFile = NULL;
    LeaveCriticalSection(&g_logOperation);
  }
  return CAN_ERR_NONE;
}

//*****************************************************************************
MMC_CAN_API int CAN_GetVehicleData (LPVEHICLE v) {
  
  memcpy(v, &theVehicle, sizeof(*v));
  return 0;
}
//*****************************************************************************
MMC_CAN_API int CAN_ResetECUErrors() {
  BYTE msg[CAN_MSG_LENGTH];
  if(!g_hPort) {
    return CAN_ERR_PORT;
  }
  if (g_comsending) {
    return CAN_ERR_BUSY;
  }
  PrepareMessage(msg, CAN_MSG_LENGTH,
    0x00, 0x00, 0x00, 0x00, 0x07, 0xE0, 0x01, 0x04,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
  );
  PortWrite(msg, CAN_MSG_LENGTH);
  return CAN_ERR_NONE;
}

//*****************************************************************************
MMC_CAN_API int CAN_ResetEngineECU (BYTE mode) {//todo: check mode meaning
  BYTE msgRqDiag[CAN_MSG_LENGTH], msgCommand[CAN_MSG_LENGTH];
  if(!g_hPort) {
    return CAN_ERR_PORT;
  }
  if (g_comsending) {
    return CAN_ERR_BUSY;
  }
  PrepareMessage(msgRqDiag, CAN_MSG_LENGTH, 
    0x00, 0x00, 0x00, 0x00, 0x07, 0xE0, 0x02, 0x10,
    0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
  );
  PrepareMessage(msgCommand, CAN_MSG_LENGTH, 
    0x00, 0x00, 0x00, 0x00, 0x07, 0xE0, 0x02, 0x11,
    mode, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
  );
  PortWrite(msgRqDiag, CAN_MSG_LENGTH);
  PortWriteWithTimeout(msgCommand, CAN_MSG_LENGTH, 300);
  return CAN_ERR_NONE;
}

//*****************************************************************************
MMC_CAN_API int CAN_Fan1On () {
  BYTE msgRqDiag[CAN_MSG_LENGTH], msgCommand[CAN_MSG_LENGTH];
  if(!g_hPort) {
    return CAN_ERR_PORT;
  }
  if (g_comsending) {
    return CAN_ERR_BUSY;
  }
  PrepareMessage(msgRqDiag, CAN_MSG_LENGTH, 
    0x00, 0x00, 0x00, 0x00, 0x07, 0xE0, 0x02, 0x10,
    0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
  );
  PrepareMessage(msgCommand, CAN_MSG_LENGTH, 
    0x00, 0x00, 0x00, 0x00, 0x07, 0xE0, 0x05, 0x2F,
    0x00, 0x0A, 0x06, 0xFF, 0x00, 0x00, 0x00, 0x00
  );
  PortWrite(msgRqDiag, CAN_MSG_LENGTH);
  PortWriteWithTimeout(msgCommand, CAN_MSG_LENGTH, 300);
  return CAN_ERR_NONE;
}

//*****************************************************************************
MMC_CAN_API int CAN_Fan1Off () {
  BYTE msgRqDiag[CAN_MSG_LENGTH], msgCommand[CAN_MSG_LENGTH];
  if(!g_hPort) {
    return CAN_ERR_PORT;
  }
  if (g_comsending) {
    return CAN_ERR_BUSY;
  }
  PrepareMessage(msgRqDiag, CAN_MSG_LENGTH, 
    0x00, 0x00, 0x00, 0x00, 0x07, 0xE0, 0x02, 0x10,
    0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
  );
  PrepareMessage(msgCommand, CAN_MSG_LENGTH, 
    0x00, 0x00, 0x00, 0x00, 0x07, 0xE0, 0x05, 0x2F,
    0x00, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
  );
  PortWrite(msgRqDiag, CAN_MSG_LENGTH);
  PortWriteWithTimeout(msgCommand, CAN_MSG_LENGTH, 300);
  return CAN_ERR_NONE;
}

//*****************************************************************************
MMC_CAN_API int CAN_Fan2On () {
  BYTE msgRqDiag[CAN_MSG_LENGTH], msgCommand[CAN_MSG_LENGTH];
  if(!g_hPort) {
    return CAN_ERR_PORT;
  }
  if (g_comsending) {
    return CAN_ERR_BUSY;
  }
  PrepareMessage(msgRqDiag, CAN_MSG_LENGTH, 
    0x00, 0x00, 0x00, 0x00, 0x07, 0xE0, 0x02, 0x10,
    0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
  );
  PrepareMessage(msgCommand, CAN_MSG_LENGTH, 
    0x00, 0x00, 0x00, 0x00, 0x07, 0xE0, 0x05, 0x2F,
    0x00, 0x0D, 0x06, 0xFF, 0x00, 0x00, 0x00, 0x00
  );
  PortWrite(msgRqDiag, CAN_MSG_LENGTH);
  PortWriteWithTimeout(msgCommand, CAN_MSG_LENGTH, 300);
  return CAN_ERR_NONE;
}

//*****************************************************************************
MMC_CAN_API int CAN_Fan2Off () {
  BYTE msgRqDiag[CAN_MSG_LENGTH], msgCommand[CAN_MSG_LENGTH];
  if(!g_hPort) {
    return CAN_ERR_PORT;
  }
  if (g_comsending) {
    return CAN_ERR_BUSY;
  }
  PrepareMessage(msgRqDiag, CAN_MSG_LENGTH, 
    0x00, 0x00, 0x00, 0x00, 0x07, 0xE0, 0x02, 0x10,
    0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
  );
  PrepareMessage(msgCommand, CAN_MSG_LENGTH, 
    0x00, 0x00, 0x00, 0x00, 0x07, 0xE0, 0x05, 0x2F,
    0x00, 0x0D, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
  );
  PortWrite(msgRqDiag, CAN_MSG_LENGTH);
  PortWriteWithTimeout(msgCommand, CAN_MSG_LENGTH, 300);
  return CAN_ERR_NONE;
}

//*****************************************************************************
MMC_CAN_API int CAN_SetIdleRPM(BYTE rpm) {
  BYTE msgRqDiag[CAN_MSG_LENGTH], msgCommand[CAN_MSG_LENGTH];
  if(!g_hPort) {
    return CAN_ERR_PORT;
  }
  if (g_comsending) {
    return CAN_ERR_BUSY;
  }
  PrepareMessage(msgRqDiag, CAN_MSG_LENGTH, 
    0x00, 0x00, 0x00, 0x00, 0x07, 0xE0, 0x02, 0x10,
    0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
  );
  PrepareMessage(msgCommand, CAN_MSG_LENGTH, 
    0x00, 0x00, 0x00, 0x00, 0x07, 0xE0, 0x05, 0x2F,
    0x00, 0x42, 0x07, rpm, 0x00, 0x00, 0x00, 0x00
  );
  PortWrite(msgRqDiag, CAN_MSG_LENGTH);
  PortWriteWithTimeout(msgCommand, CAN_MSG_LENGTH, 300);
  return CAN_ERR_NONE;
}

//*****************************************************************************
MMC_CAN_API int CAN_ResetABSErrors () {
  BYTE msgRqDiag[CAN_MSG_LENGTH], msgCommand[CAN_MSG_LENGTH];
  if(!g_hPort) {
    return CAN_ERR_PORT;
  }
  if (g_comsending) {
    return CAN_ERR_BUSY;
  }
  PrepareMessage(msgRqDiag, CAN_MSG_LENGTH, 
    0x00, 0x00, 0x00, 0x00, 0x07, 0xE3, 0x02, 0x10,
    0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
  );
  PrepareMessage(msgCommand, CAN_MSG_LENGTH, 
    0x00, 0x00, 0x00, 0x00, 0x07, 0xE3, 0x04, 0x14,
    0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00
  );
  PortWrite(msgRqDiag, CAN_MSG_LENGTH);
  PortWriteWithTimeout(msgCommand, CAN_MSG_LENGTH, 300);
  return CAN_ERR_NONE;
}
//*****************************************************************************
MMC_CAN_API int CAN_ResetSRSErrors () {
  BYTE msgRqDiag[CAN_MSG_LENGTH], msgCommand[CAN_MSG_LENGTH];
  if(!g_hPort) {
    return CAN_ERR_PORT;
  }
  if (g_comsending) {
    return CAN_ERR_BUSY;
  }
  PrepareMessage(msgRqDiag, CAN_MSG_LENGTH,
    0x00, 0x00, 0x00, 0x00, 0x07, 0xE5, 0x02, 0x10,
    0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
  );
  PrepareMessage(msgCommand, CAN_MSG_LENGTH,
    0x00, 0x00, 0x00, 0x00, 0x07, 0xE5, 0x04, 0x14,
    0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00
  );
  PortWrite(msgRqDiag, CAN_MSG_LENGTH);
  PortWriteWithTimeout(msgCommand, CAN_MSG_LENGTH, 300);
  return CAN_ERR_NONE;
}
//*****************************************************************************
MMC_CAN_API int CAN_ResetTCUErrors () {
  BYTE msgRqDiag[CAN_MSG_LENGTH], msgCommand[CAN_MSG_LENGTH];
  if(!g_hPort) {
    return CAN_ERR_PORT;
  }
  if (g_comsending) {
    return CAN_ERR_BUSY;
  }
  PrepareMessage(msgRqDiag, CAN_MSG_LENGTH,
    0x00, 0x00, 0x00, 0x00, 0x07, 0xE1, 0x02, 0x10,
    0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
  );
  PrepareMessage(msgCommand, CAN_MSG_LENGTH,
    0x00, 0x00, 0x00, 0x00, 0x07, 0xE1, 0x04, 0x14,
    0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00
  );
  PortWrite(msgRqDiag, CAN_MSG_LENGTH);
  PortWriteWithTimeout(msgCommand, CAN_MSG_LENGTH, 300);
  return CAN_ERR_NONE;
}
#ifdef __cplusplus 
}
#endif